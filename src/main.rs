use chrono::Datelike;
use std::convert::TryInto;

const HALF_LIFE_DAYS: f32 = 60.0;

#[derive(serde::Deserialize)]
struct Ladder {
	matches: Vec<Match>,
	players: std::collections::HashMap<String, Player>,
}

#[derive(serde::Deserialize)]
struct Player {
}

#[derive(serde::Deserialize)]
struct Match {
	date: chrono::DateTime<chrono::Utc>,
	parsed_score: Option<[u32; 2]>,
	players: [String; 2],
}

#[derive(Copy, Clone, Eq, Ord, PartialEq, PartialOrd)]
struct PlayerId(u32);

#[derive(Debug)]
struct Players {
	names: Vec<String>,
	matchups: Vec<Matchup>,
}

impl Players {
	fn new(players: std::collections::HashMap<String, Player>) -> Self {
		let len = players.len();
		assert!(len < u32::max_value() as usize);

		let mut names = players.into_iter()
			.map(|(k, _v)| k)
			.collect::<Vec<_>>();
		names.sort_unstable();

		let mut matchups = Vec::new();
		matchups.resize_with(len * (len - 1) / 2, Default::default);

		Players { names, matchups }
	}

	fn len(&self) -> u32 {
		self.names.len() as u32
	}

	fn ids(&self) -> impl Iterator<Item=PlayerId> + 'static {
		(0..self.names.len()).map(|i| PlayerId(i as u32))
	}

	fn matchups(&self) -> impl Iterator<Item=[PlayerId; 2]> + 'static {
		let len = self.len();
		(0..(len-1)).flat_map(move |a| ((a+1)..len).map(move |b| [PlayerId(a), PlayerId(b)]))
	}

	fn find(&self, name: &str) -> Option<PlayerId> {
		self.names.iter()
			.position(|n| n == name)
			.map(|index| PlayerId(index as u32))
	}

	fn name(&self, id: PlayerId) -> &str {
		&self.names[id.0 as usize]
	}

	fn matchup_index(&self, a: PlayerId, b: PlayerId) -> usize {
		let a = a.0 as usize;
		let b = b.0 as usize;
		debug_assert!(a < b);

		b * (b - 1) / 2 + a
	}

	fn matchup(&self, a: PlayerId, b: PlayerId) -> &Matchup {
		&self.matchups[self.matchup_index(a, b)]
	}

	fn any_matchup(&self, a: PlayerId, b: PlayerId) -> Matchup {
		if a < b {
			self.matchup(a, b).clone()
		} else {
			let mut m = self.matchup(b, a).clone();
			m.expected = MatchupInt::max_value() - m.expected;
			m
		}
	}

	fn matchup_mut(&mut self, a: PlayerId, b: PlayerId) -> &mut Matchup {
		let i = self.matchup_index(a, b);
		&mut self.matchups[i]
	}

	fn solve(&self, day: u16) -> impl Iterator<Item=(PlayerId, f32)> {
		let len = self.len() as usize;
		let mut m = nalgebra::DMatrix::from_element(len + 1, len, 0.0);
		let mut p = nalgebra::DVector::from_element(len + 1, 0.0);

		// Each player will pull on every player which they have played. They will pull the player towards the point 2*expectation-1 away from them. For example a player that has 100% expectation will push their opponent to be 1 point below them.
		// We compute a matrix to solve this. Each player will look like the
		// following.
		// push(A, B) = (expectation(A, B) - (A.pos - B.pos)) * confidence(A, B)
		// 0 = push(P, O1) + push(P, O2) + push(P, O2)

		for [a, b] in self.matchups() {
			let matchup = self.matchup(a, b);
			if matchup.games == 0 { continue }

			let ai = a.0 as usize;
			let bi = b.0 as usize;

			let expectation = 2.0*matchup.expectation() - 1.0;

			let prev_age = day - matchup.modified;
			let old_epochs = prev_age as f32 / HALF_LIFE_DAYS;
			let mut relevance = 2.0f32.powf(-old_epochs.powi(2));

			let games_confidence = 1.0 - 0.8_f32.powi(matchup.games as i32);
			relevance *= games_confidence;

			p[(ai)] += expectation * relevance;
			p[(bi)] -= expectation * relevance;

			m[(ai, ai)] += relevance;
			m[(bi, bi)] += relevance;
			m[(bi, ai)] = -relevance;
			m[(ai, bi)] = -relevance;
		}

		for i in 0..len {
			m[(len, i)] = 1.0;
		}

		// eprintln!("m:{},p{}", m, p);

		let decomp = m.svd(true, true);
		let x = decomp.solve(&p, 0.000001).expect("Linear resolution failed.");

		(0..len).map(move |id| (PlayerId(id as u32), x[id]))
	}
}

#[derive(Default, serde::Serialize)]
struct Ops {
	updates: Output,
}

#[derive(Default, serde::Serialize)]
struct Output {
	expectations: Table,
	latest_expectations: Table,
	matches: Table,
	rankings: Table,
	ranks: Table,
}

type Table = Vec<Vec<String>>;

type MatchupInt = u8;

#[derive(Clone, Debug, Default)]
struct Matchup {
	expected: MatchupInt,
	games: MatchupInt,
	modified: u16,
}

impl Matchup {
	fn expectation(&self) -> f32 {
		self.expected as f32 / MatchupInt::max_value() as f32
	}
}

fn main() {
	let file = std::fs::File::open(std::env::args().nth(1).unwrap()).unwrap();
	let reader = std::io::BufReader::new(file);
	let ladder: Ladder = serde_json::from_reader(reader).unwrap();

	assert!(ladder.matches.len() > 0);
	assert!(ladder.matches.len() <= u16::max_value() as usize);

	let mut players = Players::new(ladder.players);

	let dayzero = ladder.matches[0].date.num_days_from_ce();

	let mut output: Output = Default::default();

	let mut matchups = vec!["Date".to_string()];
	for [a, b] in players.matchups() {
		let aname = players.name(a);
		let bname = players.name(b);
		matchups.push(format!("{} vs {}", aname, bname));
	}
	output.expectations.push(matchups);

	output.matches.push(
		["Date", "Winner", "W Score", "L Score", "Loser", "Actual", "Expectation", "Surprise"].iter()
			.map(|s| s.to_string())
			.collect());

	let mut rankings = vec!["Date".to_string()];
	let mut ranks = vec!["Date".to_string()];
	for p in players.ids() {
		let name = players.name(p);
		rankings.push(name.to_string());
		ranks.push(name.to_string());
	}
	output.rankings.push(rankings);
	output.ranks.push(ranks);

	for m in ladder.matches {
		let mut score = match m.parsed_score {
			Some(score) => score,
			None => continue,
		};

		let datestr = format!("{}", m.date.format("%F %T"));
		let day = m.date.num_days_from_ce() - dayzero;
		let day: u16 = day.try_into().unwrap();

		let winner = players.find(&m.players[0]).unwrap();
		let loser = players.find(&m.players[1]).unwrap();

		let mut match_output = Vec::with_capacity(output.matches[0].len());
		match_output.push(datestr.clone());
		match_output.push(players.name(winner).to_string());
		match_output.push(format!("{}", score[0]));
		match_output.push(format!("{}", score[1]));
		match_output.push(players.name(loser).to_string());

		let [a, b] = if winner > loser {
			score.reverse();
			[loser, winner]
		} else {
			[winner, loser]
		};

		let matchup = players.matchup_mut(a, b);
		let expectation = matchup.expectation();

		let outcome = score[0] as f32 / score.iter().sum::<u32>() as f32;

		if matchup.games == 0 {
			match_output.push(String::new());
			match_output.push(String::new());
			match_output.push(String::new());
		} else if winner > loser {
			match_output.push(format!("{}", 1.0 - outcome));
			match_output.push(format!("{}", 1.0 - expectation));
			match_output.push(format!("{}", expectation - outcome));
		} else {
			match_output.push(format!("{}", outcome));
			match_output.push(format!("{}", expectation));
			match_output.push(format!("{}", outcome - expectation));
		}

		let expected = match matchup.games {
			0..=3 => {
				// Equal average.
				let new_frac = 1.0 / (matchup.games + 1) as f32;
				expectation * (1.0 - new_frac) + outcome * new_frac
			}
			_ => {
				let mut old_value = 0.75;

				// Reduce value of old games.
				let prev_age = day - matchup.modified;
				let old_epochs = prev_age as f32 / HALF_LIFE_DAYS;
				old_value *= 2.0f32.powf(-old_epochs.powi(2));

				let new_value = 1.0 - old_value;

				expectation as f32 * old_value + outcome * new_value
			}
		};
		matchup.expected = (expected * MatchupInt::max_value() as f32) as MatchupInt;
		matchup.games += 1;
		matchup.modified = day;

		let mut expectations = vec![datestr.clone()];
		for matchup in players.matchups() {
			if matchup == [a, b] {
				expectations.push(format!("{}", expected));
			} else {
				expectations.push(String::new());
			}
		}
		output.expectations.push(expectations);

		output.matches.push(match_output);

		let mut scores = players.solve(day).collect::<Vec<_>>();

		let mut rankings = vec![datestr.clone()];
		rankings.extend(scores.iter().map(|(_, s)| format!("{}", s)));
		output.rankings.push(rankings);

		let mut ranks = vec![datestr];
		scores.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());
		ranks.extend(players.ids()
			.map(|p| scores.iter().position(|s| s.0 == p).unwrap())
			.map(|r| format!("{}", r + 1)));
		output.ranks.push(ranks);
	}

	let mut latest_expectations = vec![String::new()];
	latest_expectations.extend(players.ids().map(|p| players.name(p).to_string()));
	output.latest_expectations.push(latest_expectations);
	for a in players.ids() {
		let mut row = vec![players.name(a).to_string()];
		for b in players.ids() {
			if a == b {
				row.push(String::new());
				continue
			}

			let matchup = players.any_matchup(a, b);
			if matchup.games == 0 {
				row.push(String::new());
				continue
			}

			row.push(format!("{}", matchup.expectation()));
		}
		output.latest_expectations.push(row);
	}

	output.expectations[1..].reverse();
	output.matches[1..].reverse();
	output.rankings[1..].reverse();
	output.ranks[1..].reverse();
	let ops = Ops { updates: output };
	serde_json::to_writer_pretty(std::io::stdout(), &ops).unwrap();
}
