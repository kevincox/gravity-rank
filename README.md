# Gravity Rank

A ranking algorithm which aims to compute an accurate ordering of a fairly small set of players.

## Goals

- Accurately order players.
- Produce good results even when players have varying amount of matches.
	- Players with low-quality data may have low-quality results but should have minimal effect on players with good data.

## Assumptions

### Small set of players.

It is okay if the algorithm sales poorly with the number of players. In particular <1000 players should work however we don't need to support more than that. If you want to rank all of the players of an online community it may be too slow. See [Performance](#Performance) for details.

### Absolute Skill

This assumes that skill can be represented by a single value and that outcomes of matches can be predicted by the difference in skill between two players.

This is a simplification as often you will have cycles of players which can beat each other. In this case the relative ranking of these players will not be particularly meaningful however the ranking of the group compared to the rest of the players should remain useful.

### Players Change Over Time

The algorithm will put more weight on recent games.

## Algorithm

### Matchups

Games are used to produce matchups. A matchup is a relationship between two players. Each matchup has the following information.

- Player A
- Player B
- Expectation. This is a percentage, representing the average outcome of a match between these two players.
- Confidence. This is a number representing the confidence in the expectation. This a relative number.

Note: Matchups are symmetric. If "A vs B" has expectation 0.7 then "B vs A" is assumed to have an expectation of 0.3

The current algorithm uses a simple decaying average. The result of each new match replaces at least 25% of the expectation. There weight of the latest match will be higher the older the previous match is.

### Scoring

Using the matchups the players are assigned values. The values are calculated by solving a system of forces where each matchup applies a force.

Each matchup will attempt to keep the two involved players `Expectation` distance apart. The force applied will be the difference between the expectation and the score difference. This force will be adjusted by the confidence.

```python
def force(a, b):
  score_difference = score(a) - score(b)
  expectation_difference = score_difference - expectation(a, b)
  return expectation_difference * confidence(a, b)

solve sum(force(player, opponent) for opponent in opponents) == 0
```

These forces will be solved to find the scores of the players which balance the forces.

## Performance

Performance is broken into two main components. Computing matchups and scoring.

### Matchups

The matchup calculation is `O(n)` with the number of games and fully incremental. So adding a new game is an `O(1)` operation. The current implementation is a simple decaying average with 4 bytes of state per matchup.

The algorithm could be adjusted to consider the entire matchup history when computing the score, making it non-incremental. However the value of looking far back dimities very quickly so even if it is adjusted to look back a bit in history the cost will be small.

### Scoring

Scoring takes all matchups into consideration and such dominates the computation. The system of forces can be modeled as a linear system of `N` variables and `N+1` equations where `N` is the number of players.

The current implementation solves this as an `N x N+1` matrix. The matrix is fairly sparse. It has 2 entires per matchup and one entry for each active player.
